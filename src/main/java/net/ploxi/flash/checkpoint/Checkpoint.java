package net.ploxi.flash.checkpoint;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.Expose;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ploxi.flash.player.FlashPlayer;
import net.ploxi.flash.util.Cuboid;
import org.bukkit.Location;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author RewisServer Development
 */
public class Checkpoint implements JsonSerializer<Checkpoint>, JsonDeserializer<Checkpoint> {

	private static final Gson GSON = new GsonBuilder()
			.excludeFieldsWithoutExposeAnnotation()
			.serializeNulls()
			.setPrettyPrinting()
			.create();

	@Expose private final Set<Cuboid> cuboids;
	@Expose private final int order;
	@Expose private final Map<PotionEffectType, Integer> potionEffectTypes;
	@Expose private final boolean finisher;

	public Checkpoint(Set<Cuboid> cuboids, int order, Map<PotionEffectType, Integer> potionEffectTypes, boolean finisher) {
		this.cuboids = cuboids;
		this.order = order;
		this.potionEffectTypes = potionEffectTypes;
		this.finisher = finisher;
	}

	public Checkpoint() {
		this.cuboids = new HashSet<>();
		this.order = 0;
		this.potionEffectTypes = new HashMap<>();
		this.finisher = false;
	}

	@Override
	public Checkpoint deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
		JsonObject obj = je.getAsJsonObject();

		int order = obj.get("order").getAsInt();
		boolean finisher = obj.get("finisher").getAsBoolean();
		Set<Cuboid> cuboids = new HashSet<>();

		{
			JsonArray array = obj.getAsJsonArray("cuboids");
			array.iterator().forEachRemaining(jsonElement -> {
				Map<String, Object> map = GSON.fromJson(jsonElement, new TypeToken<Map<String, Object>>() {
				}.getType());
				Map<String, Object> newMap = new HashMap<>();
				// Gonna rework this whole shit
				map.forEach((key, value) -> {
					newMap.put(key, value instanceof Double ? (int) (double) (Double) value : value);
				});
				
				cuboids.add(new Cuboid(newMap));
			});
		}
		Map<PotionEffectType, Integer> potionEffectTypes = new HashMap<>();
		{
			Map<String, Integer> map = GSON.fromJson(obj.get("potionEffectTypes").getAsJsonObject(), new TypeToken<Map<String, Integer>>() {
			}.getType());
			map.forEach((t, u) -> potionEffectTypes.put(PotionEffectType.getByName(t), u));
		}

		return new Checkpoint(cuboids, order, potionEffectTypes, finisher);
	}

	@Override
	public JsonElement serialize(Checkpoint t, Type type, JsonSerializationContext jsc) {
		JsonObject obj = new JsonObject();

		obj.addProperty("order", t.order);
		obj.addProperty("finisher", t.finisher);

		{
			JsonArray array = new JsonArray();
			for (Cuboid cuboid : t.cuboids) {
				array.add(GSON.toJsonTree(cuboid.serialize(), new TypeToken<Map<String, Object>>() {
				}.getType()));
			}
			obj.add("cuboids", array);
		}
		{
			Map<String, Integer> potionEffects = new HashMap<>();
			t.potionEffectTypes.forEach((typ, inte) -> potionEffects.put(typ.getName(), inte));
			obj.add("potionEffectTypes", GSON.toJsonTree(potionEffects, new TypeToken<Map<String, Integer>>() {
			}.getType()));
		}

		return obj;
	}

	/**
	 * Checks if the given {@link Location} is in any of the {@link Checkpoint#cuboids cuboids}
	 *
	 * @param loc The {@link Location} to check
	 * @return True if the given {@link Location location} is in any of the {@link Checkpoint#cuboids cuboids}
	 */
	public boolean contains(Location loc) {
		return this.getCuboids().stream().anyMatch((Cuboid cuboid) -> cuboid.contains(loc));
	}

	/**
	 * Called when a {@link FlashPlayer} is contained in {@link Checkpoint#contains(org.bukkit.Location)
	 *
	 * @param flashPlayer The {@link FlashPlayer}
	 */
	public void onEnter(FlashPlayer flashPlayer) {
		flashPlayer.getPlayer().getActivePotionEffects().forEach(pot -> flashPlayer.getPlayer().removePotionEffect(pot.getType()));
		this.potionEffectTypes.forEach((PotionEffectType type, Integer amplifier) -> {
			flashPlayer.getPlayer().addPotionEffect(new PotionEffect(type, 10000000, amplifier, false, false), true);
		});
	}

	/**
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @return the finisher
	 */
	public boolean isFinisher() {
		return finisher;
	}

	/**
	 * @return the cuboids
	 */
	public Set<Cuboid> getCuboids() {
		return cuboids;
	}

}
