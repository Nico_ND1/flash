package net.ploxi.flash;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ploxi.flash.commands.AddCheckpointCommand;
import net.ploxi.flash.commands.RegisterMapCommand;
import net.ploxi.flash.config.FlashGameConfig;
import net.ploxi.flash.manager.GameManager;
import net.ploxi.flash.manager.LobbyManager;
import net.ploxi.flash.manager.Manager;
import net.ploxi.flash.manager.MapManager;
import net.ploxi.flash.manager.MapVotingManager;
import net.ploxi.flash.manager.PlayerManager;
import net.ploxi.flash.player.FlashPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author RewisServer Development
 */
public class FlashPlugin extends JavaPlugin {

	public static enum GameState {

		LOADING, LOBBY, LOBBY_COUNTDOWN, INGAME_COUNTDOWN, INGAME, END;

		/**
		 * Check if this {@link GameState} is wether {@link GameState#INGAME} or {@link GameState#INGAME_COUNTDOWN}
		 *
		 * @return True if this {@link GameState} is {@link GameState#INGAME} or {@link GameState#INGAME_COUNTDOWN}
		 */
		public boolean isIngame() {
			return this == INGAME_COUNTDOWN || this == INGAME;
		}

		public static GameState[] ingameStates() {
			return new GameState[]{INGAME, INGAME_COUNTDOWN};
		}

		/**
		 * Check if this {@link GameState} is wether {@link GameState#LOBBY} or {@link GameState#LOBBY_COUNTDOWN}
		 *
		 * @return True if this {@link GameState} is {@link GameState#LOBBY} or {@link GameState#LOBBY_COUNTDOWN}
		 */
		public boolean isLobby() {
			return this == LOBBY || this == LOBBY_COUNTDOWN;
		}

		public static GameState[] lobbyStates() {
			return new GameState[]{LOBBY, LOBBY_COUNTDOWN};
		}

		/**
		 * Checks if this {@link GameState} is {@link GameState#LOADING}
		 *
		 * @return True if this {@link GameState} isn't {@link GameState#LOADING}
		 */
		public boolean isJoinable() {
			return this != LOADING;
		}

	}

	private final Set<FlashPlayer> players = new HashSet<>();
	private final Map<String, Manager> manager = new HashMap<>();
	private GameState state = GameState.LOADING;
	private FlashGameConfig gameConfig;

	@Override
	public void onEnable() {
		try {
			if (new File("plugins/Flash/gameConfig.json").createNewFile())
				new FlashGameConfig().save();
		} catch (IOException ex) {
			Logger.getLogger(FlashPlugin.class.getName()).log(Level.SEVERE, null, ex);
		}

		this.getCommand("registermap").setExecutor(new RegisterMapCommand());
		this.getCommand("addcheckpoint").setExecutor(new AddCheckpointCommand());
		this.gameConfig = FlashGameConfig.readFromConfig();

		this.registerManager("Player", new PlayerManager());
		this.registerManager("Lobby", new LobbyManager());
		this.registerManager("Game", new GameManager());
		this.registerManager("Maps", new MapManager());
		this.registerManager("Voting", new MapVotingManager());

		this.switchState(GameState.LOBBY);
	}

	/**
	 * Switches the {@link FlashPlugin#state} to a new {@link GameState}
	 *
	 * @param newState The new {@link GameState} to set
	 */
	public void switchState(GameState newState) {
		this.manager.forEach((name, manager) -> {
			if (Arrays.stream(manager.activatedOn()).anyMatch(gameState -> gameState == newState)) {
				manager.switchState(this.getState(), newState);
				manager.onLoad();
				if (newState == GameState.INGAME)
					manager.gameStart();
				if (newState == GameState.END)
					manager.gameStop();
			} else {
				manager.unregisterAllListener();
				manager.onUnload();
			}
		});
		this.state = newState;
	}

	/**
	 * Gets a player from {@link FlashPlugin#manager} and adds a new one if it's not existing
	 *
	 * @param player The player to get from
	 * @return The {@link FlashPlayer} from the given {@link Player}
	 */
	public FlashPlayer getPlayer(Player player) {
		FlashPlayer flashPlayer
				= this.players.stream().filter(flashPlayer2 -> flashPlayer2.getPlayer().getUniqueId().equals(player.getUniqueId())).findAny().orElse(null);

		if (flashPlayer != null)
			return flashPlayer;
		return this.addPlayer(player);
	}

	/**
	 * Adds a player to {@link FlashPlugin#players}
	 *
	 * @param player The player to add
	 * @return The {@link FlashPlayer} from the {@link Player}
	 */
	protected FlashPlayer addPlayer(Player player) {
		FlashPlayer flashPlayer = new FlashPlayer(player);
		this.players.add(flashPlayer);
		return flashPlayer;
	}

	/**
	 * Puts a manager into {@link FlashPlugin#manager}
	 *
	 * @param name The name of the manager
	 * @param manager The manager to register
	 */
	public void registerManager(String name, Manager manager) {
		Logger.getLogger(FlashPlugin.class.getName()).log(Level.INFO, "Registering manager ({0})", name);
		if (this.manager.containsKey(name))
			return;
		this.manager.putIfAbsent(name, manager);
		manager.onLoad();

		if (this.getState().isIngame())
			manager.gameStart();
		if (this.getState() == GameState.END)
			manager.gameStop();
	}

	/**
	 * Gets a {@link Manager} from {@link FlashPlugin#manager}
	 *
	 * @param <T> The type of the manager
	 * @param name The name of the manager
	 * @param clazz The class of the manager
	 * @return A casted manager class
	 */
	public <T> T getManager(String name, Class<T> clazz) {
		return clazz.cast(this.manager.get(name));
	}

	/**
	 * @return the players
	 */
	public Set<FlashPlayer> getPlayers() {
		return players;
	}

	/**
	 * @return the gameConfig
	 */
	public FlashGameConfig getGameConfig() {
		return gameConfig;
	}

	/**
	 * @param gameConfig the gameConfig to set
	 */
	public void setGameConfig(FlashGameConfig gameConfig) {
		this.gameConfig = gameConfig;
	}

	/**
	 * @return the state
	 */
	public GameState getState() {
		return state;
	}

}
