package net.ploxi.flash.commands;

import com.google.common.collect.ImmutableSet;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.map.FlashMap;
import net.ploxi.flash.util.ConfigurableLocation;
import net.ploxi.flash.util.Cuboid;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author RewisServer Development
 */
public class RegisterMapCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player))
			return true;
		final Player player = (Player) sender;

		/*
		registermap <Name> <Author> <Speed> <DieAt> <WORLD,X1,Y1,Z1,X2,Y2,Z2,;...>
		 */
		if (args.length == 5) {
			String mapName = args[0];
			String author = args[1];
			float speed = -1;
			int dieAt = -1;
			try {
				speed = Float.valueOf(args[2]);
				dieAt = Integer.valueOf(args[3]);
			} catch (NumberFormatException exception) {
				player.sendMessage(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix() + " §cSchreibe eine Zahl.");
				return true;
			}

			String[] split = args[4].split(",");
			String worldName = split[0];
			int x1 = Integer.valueOf(split[1]);
			int y1 = Integer.valueOf(split[2]);
			int z1 = Integer.valueOf(split[3]);
			int x2 = Integer.valueOf(split[4]);
			int y2 = Integer.valueOf(split[5]);
			int z2 = Integer.valueOf(split[6]);

			FlashMap flashMap = new FlashMap(mapName, author, speed, player.getWorld().getName(),
					new ConfigurableLocation(player.getLocation()), ImmutableSet.of(), dieAt,
					new Cuboid(Bukkit.getWorld(worldName), x1, y1, z1, x2, y2, z2).serialize());
			flashMap.save();

			player.sendMessage(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix() + " §aDie Map wurde gespeichert.");

			return true;
		}
		player.sendMessage(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix() + " §cBenutzung: /" + label + " <Name> <Author> <Speed>");

		return true;
	}

}
