package net.ploxi.flash.commands;

import com.google.common.collect.ImmutableSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.checkpoint.Checkpoint;
import net.ploxi.flash.manager.MapManager;
import net.ploxi.flash.map.FlashMap;
import net.ploxi.flash.util.Cuboid;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author RewisServer Development
 */
public class AddCheckpointCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		/*
		<WORLD,X1,Y1,Z1,X2,Y2,Z2,;...> <Order> <PotionEffectType:Integer;...> <IsFinisher> <MapName>
		 */

		if (args.length == 5) {
			Set<Cuboid> sets = new HashSet<>();
			{
				String cuboidString = args[0];
				for (String cuboidS : cuboidString.split(";")) {
					String[] split = cuboidS.split(",");
					String worldName = split[0];
					int x1 = Integer.valueOf(split[1]);
					int y1 = Integer.valueOf(split[2]);
					int z1 = Integer.valueOf(split[3]);
					int x2 = Integer.valueOf(split[4]);
					int y2 = Integer.valueOf(split[5]);
					int z2 = Integer.valueOf(split[6]);
					sets.add(new Cuboid(Bukkit.getWorld(worldName), x1, y1, z1, x2, y2, z2));
				}
			}
			int order = Integer.valueOf(args[1]);
			Map<PotionEffectType, Integer> potionEffects = new HashMap<>();
			{
				for (String potS : args[2].split(";")) {
					String[] split = potS.split(":");
					potionEffects.put(PotionEffectType.getByName(split[0]), Integer.valueOf(split[1]));
				}
			}
			boolean finisher = Boolean.valueOf(args[3]);
			String mapName = args[4];

			Checkpoint checkpoint = new Checkpoint(sets, order, potionEffects, finisher);
			FlashMap map = FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getMap(mapName);

			if (map == null) {
				sender.sendMessage(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix() + " §cDie Map wurde nicht gefunden");
				return true;
			}

			Set<Checkpoint> sets2 = new HashSet<>();
			map.getCheckpoints().iterator().forEachRemaining(checkpoints -> sets2.add(checkpoints));
			sets2.add(checkpoint);
			map.setCheckpoints(ImmutableSet.copyOf(sets2));
			map.save();

			sender.sendMessage(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix() + " §aDer Checkpoint wurde hinzugefügt.");
		}
		return true;
	}

}
