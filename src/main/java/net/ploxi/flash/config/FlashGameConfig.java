package net.ploxi.flash.config;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.ChatColor;

/**
 *
 * @author RewisServer Development
 */
public class FlashGameConfig implements JsonDeserializer<FlashGameConfig> {

	private static final Gson GSON = new GsonBuilder()
			.excludeFieldsWithoutExposeAnnotation()
			.serializeNulls()
			.setPrettyPrinting()
			.registerTypeAdapter(FlashGameConfig.class, new FlashGameConfig())
			.create();

	public static FlashGameConfig readFromConfig() {
		try {
			return GSON.fromJson(new InputStreamReader(new FileInputStream(new File("plugins/Flash/gameConfig.json")), Charset.forName("UTF-8")), FlashGameConfig.class);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(FlashGameConfig.class.getName()).log(Level.SEVERE, null, ex);
		}
		return new FlashGameConfig();
	}

	@Expose private final int minPlayers;
	@Expose private final int maxPlayers;
	@Expose private final int countdown;
	@Expose private final int[] counts;
	@Expose private final String prefix;

	public FlashGameConfig(int minPlayers, int maxPlayers, int countdown, int[] counts, String prefix) {
		this.minPlayers = minPlayers;
		this.maxPlayers = maxPlayers;
		this.countdown = countdown;
		this.counts = counts;
		this.prefix = prefix;
	}

	public FlashGameConfig() {
		this.minPlayers = 2;
		this.maxPlayers = 8;
		this.countdown = 15;
		this.counts = new int[]{60, 50, 40, 30, 20, 15, 10, 5, 4, 3, 2, 1};
		this.prefix = "&7[&bFlash&7]";
	}

	@Override
	public FlashGameConfig deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
		JsonObject jsonObject = je.getAsJsonObject();
		int minPlayers = jsonObject.get("minPlayers").getAsInt();
		int maxPlayers = jsonObject.get("maxPlayers").getAsInt();
		int countdown = jsonObject.get("countdown").getAsInt();
		List<JsonElement> elements = ImmutableList.copyOf(jsonObject.get("counts").getAsJsonArray().iterator());
		int[] counts = new int[elements.size()];
		Iterator<JsonElement> it = jsonObject.get("counts").getAsJsonArray().iterator();
		for(int i = 0; i < elements.size(); i++) {
			counts[i] = it.next().getAsInt();
		}
		String prefix = ChatColor.translateAlternateColorCodes('&', jsonObject.get("prefix").getAsString());

		return new FlashGameConfig(minPlayers, maxPlayers, countdown, counts, prefix);
	}

	public void save() {
		new File("plugins/Flash").mkdirs();
		try (FileWriter writer = new FileWriter(new File("plugins/Flash/gameConfig.json"))) {
			writer.write(GSON.toJson(this, FlashGameConfig.class));
			writer.flush();
		} catch (IOException ex) {
			Logger.getLogger(FlashGameConfig.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * @return the minPlayers
	 */
	public int getMinPlayers() {
		return minPlayers;
	}

	/**
	 * @return the maxPlayers
	 */
	public int getMaxPlayers() {
		return maxPlayers;
	}

	/**
	 * @return the countdown
	 */
	public int getCountdown() {
		return countdown;
	}

	/**
	 * @return the counts
	 */
	public int[] getCounts() {
		return counts;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

}
