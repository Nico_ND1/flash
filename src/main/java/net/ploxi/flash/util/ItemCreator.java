package net.ploxi.flash.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class ItemCreator {

	public static final ItemStack PLACEHOLDER = new ItemCreator().material(Material.STAINED_GLASS_PANE).data((short) 7).displayName("§8").build();

	private Material mat;
	private Short data;
	private Integer amount;
	private String display;
	private List<String> lore;
	private Map<Enchantment, Integer> enchantments = new HashMap<>();
	private boolean hideFlag;
	private Color color = null;

	public ItemCreator() {
	}

	public ItemCreator(ItemStack item) {
		hideFlag();
		this.mat = item.getType();
		this.data = item.getDurability();
		this.amount = item.getAmount();
		if (item.hasItemMeta()) {
			if (item.getItemMeta().hasDisplayName()) {
				this.display = item.getItemMeta().getDisplayName();
			}

			if (item.getItemMeta().hasLore()) {
				this.lore = item.getItemMeta().getLore();
			}

			if (item.getItemMeta().hasEnchants()) {
				this.enchantments = item.getItemMeta().getEnchants();
			}
		}

	}

	public ItemCreator material(Material mat) {
		this.mat = mat;
		return this;

	}

	public static void removeAllEnchants(ItemStack itemstack) {
		ItemMeta meta = itemstack.getItemMeta();
		meta.getEnchants().clear();
		itemstack.setItemMeta(meta);
	}

	public ItemCreator hideFlag() {
		this.hideFlag = true;
		return this;
	}

	public ItemCreator data(short data) {
		this.data = Short.valueOf(data);
		return this;
	}

	public ItemCreator amount(int amount) {
		this.amount = Integer.valueOf(amount);
		return this;
	}

	public ItemCreator displayName(String display) {
		this.display = display;
		return this;
	}

	public ItemCreator lore(List<String> lore) {
		this.lore = lore;
		return this;
	}

	public ItemCreator lore(String... lore) {
		this.lore = Arrays.asList(lore);
		return this;
	}

	public ItemCreator color(Color color) {
		this.color = color;
		return this;
	}

	public ItemCreator addEnchant(Enchantment ench, int level) {
		this.enchantments.put(ench, level);
		return this;
	}

	public ItemCreator removeEnchant(Enchantment ench) {
		if (!this.enchantments.containsKey(ench)) {
			return this;
		} else {
			this.enchantments.remove(ench);
			return this;
		}
	}

	public Material getMaterial() {
		return this.mat;
	}

	public Short getData() {
		return this.data;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public String getDisplayName() {
		return this.display;
	}

	public List<String> getLore() {
		return this.lore;
	}

	public Map<Enchantment, Integer> getEnchantments() {
		return this.enchantments;
	}

	public ItemStack build() {
		ItemStack item = null;
		if (this.mat == null) {
			return item;
		} else {
			if (this.data == null) {
				this.data = (short) 0;
			}

			if (this.amount == null) {
				this.amount = 1;
			}

			item = new ItemStack(this.mat, this.amount, this.data.shortValue());

			if (this.display != null || this.lore != null) {
				if (color != null) {
					LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
					if (this.display != null) {
						meta.setDisplayName(this.display);
					}
					meta.setColor(color);
					if (this.lore != null) {
						meta.setLore(this.lore);
					}

					if (this.hideFlag) {
						meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
						meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
						meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
						meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
						meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
						meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
					}

					item.setItemMeta(meta);
				} else {
					ItemMeta meta = item.getItemMeta();
					if (this.display != null) {
						meta.setDisplayName(this.display);
					}

					if (this.lore != null) {
						meta.setLore(this.lore);
					}

					if (this.hideFlag) {
						meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
						meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
						meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
						meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
						meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
						meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
					}

					item.setItemMeta(meta);
				}
			}

			if (this.enchantments.size() > 0) {
				item.addUnsafeEnchantments(this.enchantments);
			}

			return item;
		}
	}
}
