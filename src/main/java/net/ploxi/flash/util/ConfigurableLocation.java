package net.ploxi.flash.util;

import com.google.gson.annotations.Expose;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 *
 * @author RewisServer Development
 */
public class ConfigurableLocation {
	
	@Expose private final double x, y, z;
	@Expose private final float yaw, pitch;
	@Expose private final String world;

	public ConfigurableLocation(double x, double y, double z, float yaw, float pitch, String world) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.world = world;
	}
	
	public ConfigurableLocation(Location loc) {
		this.x = loc.getX();
		this.y = loc.getY();
		this.z = loc.getZ();
		this.yaw = loc.getYaw();
		this.pitch = loc.getPitch();
		this.world = loc.getWorld().getName();
	}
	
	public Location toLocation() {
		return new Location(Bukkit.getWorld(this.world), this.x, this.y, this.z, this.yaw, this.pitch);
	}

}
