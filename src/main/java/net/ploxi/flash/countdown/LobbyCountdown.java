package net.ploxi.flash.countdown;

import java.util.Arrays;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.manager.MapManager;
import net.ploxi.flash.manager.MapVotingManager;
import org.bukkit.Bukkit;

/**
 *
 * @author RewisServer Development
 */
public class LobbyCountdown extends Countdown {

	public LobbyCountdown() {
		super(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getCountdown());
	}

	@Override
	public void onFinish() {
		FlashPlugin.getPlugin(FlashPlugin.class).switchState(FlashPlugin.GameState.INGAME);
	}

	@Override
	public void onStop() {
		Bukkit.broadcastMessage(String.format("%s §cDer Countdown wurde abgebrochen!",
				FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix()));
		FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).setActualMap(null);
		FlashPlugin.getPlugin(FlashPlugin.class).getPlayers().forEach(flashPlayer -> flashPlayer.setVotedMap(null));
		FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapVotingManager.class).updateInventorys();
	}

	@Override
	public void onTick(int count) {
		if (!Arrays.stream(FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getCounts()).anyMatch(counte -> counte == count))
			return;

		if (count == 10)
			FlashPlugin.getPlugin(FlashPlugin.class).getManager("Voting", MapVotingManager.class).pollVoting();

		Bukkit.broadcastMessage(String.format("%s §7Das Spiel beginnt in §e%s §7Sekunde%s!",
				FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix(), count == 1 ? "einer" : count, count == 1 ? "" : "n"));
	}

	@Override
	public void onStart() {
		FlashPlugin.getPlugin(FlashPlugin.class).switchState(FlashPlugin.GameState.LOBBY_COUNTDOWN);
	}

}
