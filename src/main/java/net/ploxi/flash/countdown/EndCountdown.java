package net.ploxi.flash.countdown;

import java.util.Arrays;
import net.ploxi.flash.FlashPlugin;
import org.bukkit.Bukkit;

/**
 *
 * @author RewisServer Development
 */
public class EndCountdown extends Countdown {

	private static final int[] COUNTS = new int[]{10, 5, 4, 3, 2, 1};

	public EndCountdown() {
		super(10);
	}

	@Override
	public void onFinish() {
		Bukkit.shutdown();
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onTick(int count) {
		if (!Arrays.stream(COUNTS).anyMatch(counts -> counts == count))
			return;
		Bukkit.broadcastMessage(String.format("%s §cDer Server stoppt in §e%s Sekunde%s§c.",
				FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix(), count == 1 ? "einer" : count, count == 1 ? "" : "n"));
	}

	@Override
	public void onStart() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			player.teleport(Bukkit.getWorld("world").getSpawnLocation());
			player.setWalkSpeed(0.2f);
			player.getActivePotionEffects().forEach(pot -> player.removePotionEffect(pot.getType()));
		});
	}

}
