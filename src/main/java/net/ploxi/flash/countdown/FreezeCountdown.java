package net.ploxi.flash.countdown;

import java.util.HashMap;
import java.util.Map;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.manager.GameManager;
import net.ploxi.flash.manager.MapManager;
import net.ploxi.flash.player.FlashPlayer;
import net.ploxi.flash.util.Cuboid;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author RewisServer Development
 */
public class FreezeCountdown extends Countdown implements Listener {

	private final Map<Player, Block> blockMap = new HashMap<>();

	public FreezeCountdown() {
		super(10);
	}

	@Override
	public void onFinish() {
		FlashPlugin.getPlugin(FlashPlugin.class).getManager("Game", GameManager.class).unregisterListener(this);
		this.blockMap.clear();
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onTick(int count) {
		if (count == 0)
			return;
		Bukkit.broadcastMessage(String.format("%s §7Es geht in §e%s Sekunde%s §7los.", FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix(),
				count == 1 ? "einer" : count, count == 1 ? "" : "n"));
	}

	@Override
	public void onStart() {
		FlashPlugin.getPlugin(FlashPlugin.class).getManager("Game", GameManager.class).registerListener(this);
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		final FlashPlayer flashPlayer = FlashPlugin.getPlugin(FlashPlugin.class).getPlayer(player);
		if(!flashPlayer.isIngame())
			return;
		final Block block = event.getTo().getBlock();
		final Cuboid cuboid = FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getActualMap().getFreezeCuboid();
		Block oldBlock = this.blockMap.getOrDefault(player, null);
		if (oldBlock == null) {
			this.blockMap.getOrDefault(player, block);
			oldBlock = event.getFrom().getBlock();
		}
		if (block.equals(oldBlock))
			return;

		this.blockMap.put(player, block);
		
		if(!cuboid.contains(player.getLocation()))
			player.teleport(player.getWorld().getHighestBlockAt(cuboid.getCenter()).getLocation());
	}

}
