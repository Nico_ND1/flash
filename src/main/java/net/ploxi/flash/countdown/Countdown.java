package net.ploxi.flash.countdown;

import net.ploxi.flash.FlashPlugin;
import org.bukkit.Bukkit;

/**
 * A class to make easy countdowns ingame
 *
 * @author RewisServer Development
 */
public abstract class Countdown implements Runnable {

	private int count;
	private final int backupCount;
	private int taskId = -1;

	public Countdown(int count) {
		this.count = count;
		this.backupCount = count;
	}

	public boolean isRunning() {
		return this.taskId != -1;
	}

	public void start() {
		if (this.taskId == -1) {
			this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(FlashPlugin.getPlugin(FlashPlugin.class), this, 20, 20);
			this.onStart();
		}
	}

	public void start(int schedule) {
		if (this.taskId == -1) {
			this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(FlashPlugin.getPlugin(FlashPlugin.class), this, 0, schedule);
			this.onStart();
		}
	}

	public boolean stop() {
		if (!this.isRunning())
			return false;
		Bukkit.getScheduler().cancelTask(this.taskId);
		this.taskId = -1;
		return true;
	}

	public void stop(boolean force) {
		if (this.stop() && force)
			this.onStop();
	}

	@Override
	public void run() {
		if (this.getCount() == 0) {
			this.stop(false);
			this.onFinish();
		}

		this.onTick(this.getCount());
		if (this.count != -1)
			this.setCount(this.getCount() - 1);
	}

	public void resetCount() {
		this.count = this.backupCount;
	}

	public abstract void onFinish();

	public abstract void onStop();

	public abstract void onTick(int count);

	public abstract void onStart();

	/**
	 * @return the count
	 */
	public int getCount() {
		return this.count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

}
