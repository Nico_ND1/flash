package net.ploxi.flash.countdown;

import net.ploxi.flash.FlashPlugin;
import org.bukkit.Bukkit;

/**
 *
 * @author RewisServer Development
 */
public class LobbyNoPlayersCountdown extends Countdown {
	public LobbyNoPlayersCountdown() {
		super(-1);
	}

	@Override
	public void onFinish() {
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onTick(int count) {
		if(Bukkit.getOnlinePlayers().size() >= FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getMinPlayers()) {
			this.stop(true);
			return;
		}
		
		final int neededPlayers = FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getMinPlayers() - Bukkit.getOnlinePlayers().size();
		Bukkit.broadcastMessage(String.format("%s §cEs fehl%s noch §e%s Spieler§c!",
				FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix(), neededPlayers == 1 ? "t" : "en",
				neededPlayers == 1 ? "ein"
				: neededPlayers));
	}

	@Override
	public void onStart() {
		FlashPlugin.getPlugin(FlashPlugin.class).switchState(FlashPlugin.GameState.LOBBY);
	}

}
