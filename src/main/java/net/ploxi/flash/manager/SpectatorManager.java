package net.ploxi.flash.manager;

import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.player.FlashPlayer;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author RewisServer Development
 */
public class SpectatorManager extends Manager implements Listener {
	@Override
	public FlashPlugin.GameState[] activatedOn() {
		return FlashPlugin.GameState.ingameStates();
	}

	@Override
	public void gameStart() {
		this.registerListener(this);
	}

	@Override
	public void gameStop() {
	}

	@Override
	public void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState) {
	}

	@Override
	public void onLoad() {
	}

	@Override
	public void onUnload() {
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();

		if (FlashPlugin.getPlugin(FlashPlugin.class).getState().isIngame()) {
			player.setGameMode(GameMode.SPECTATOR);
			event.setJoinMessage("");
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onQuit(PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final FlashPlayer flashPlayer = FlashPlugin.getPlugin(FlashPlugin.class).getPlayer(player);

		if (flashPlayer.isIngame())
			return;

		event.setQuitMessage("");
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		final Player player = event.getPlayer();
		final FlashPlayer flashPlayer = FlashPlugin.getPlugin(FlashPlugin.class).getPlayer(player);

		if (!FlashPlugin.getPlugin(FlashPlugin.class).getState().isIngame())
			return;
		if (flashPlayer.isIngame())
			return;
		
		event.setCancelled(true);
	}

}
