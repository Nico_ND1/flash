package net.ploxi.flash.manager;

import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.player.FlashPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author RewisServer Development
 */
public class PlayerManager extends Manager implements Listener {
	@Override
	public void onLoad() {
		this.registerListener(this);
	}

	@Override
	public void onUnload() {
	}

	@Override
	public void gameStart() {
	}

	@Override
	public void gameStop() {
	}

	@Override
	public void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState) {
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final FlashPlayer flashPlayer = FlashPlugin.getPlugin(FlashPlugin.class).getPlayer(player);
		
		synchronized(FlashPlugin.getPlugin(FlashPlugin.class).getPlayers()) {
			FlashPlugin.getPlugin(FlashPlugin.class).getPlayers().remove(flashPlayer);
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onConsume(PlayerItemConsumeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onPickupItem(PlayerPickupItemEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onPortal(PlayerPortalEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onAchievement(PlayerAchievementAwardedEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onArmorStand(PlayerArmorStandManipulateEvent event) {
		event.setCancelled(true);
	}

}
