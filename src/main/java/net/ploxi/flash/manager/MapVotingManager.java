package net.ploxi.flash.manager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.map.FlashMap;
import net.ploxi.flash.player.FlashPlayer;
import net.ploxi.flash.util.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author RewisServer Development
 */
public class MapVotingManager extends Manager implements Listener {

	private static final String INVENTORY_NAME = "§aMap-Voting";
	private static final String ITEM_NAME = "§eStimme für eine Map";
	private final Inventory inventory;
	private final Map<Integer, FlashMap> slotOfMap = new HashMap<>();

	public MapVotingManager() {
		final int maps = FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getMaps().size();
		final int size = (maps / 9 + (maps % 9 == 0 ? 0 : 1)) * 9;
		this.inventory = Bukkit.createInventory(null, size, INVENTORY_NAME);

		FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getMaps().forEach(flashMap -> {
			int slot = this.inventory.firstEmpty();
			this.slotOfMap.put(slot, flashMap);
			this.inventory.setItem(slot, new ItemCreator().material(Material.PAPER).displayName(flashMap.getName())
					.lore("§eErbauer: §a" + flashMap.getAuthor(), "§eVotes: §a0")
					.build());
		});
	}

	@Override
	public FlashPlugin.GameState[] activatedOn() {
		return FlashPlugin.GameState.lobbyStates();
	}

	@Override
	public void onLoad() {
		this.registerListener(this);
	}

	@Override
	public void onUnload() {
	}

	@Override
	public void gameStart() {
		if (FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getActualMap() == null)
			this.pollVoting();
	}

	@Override
	public void gameStop() {
	}

	@Override
	public void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState) {
		switch (oldState) {
			case LOBBY_COUNTDOWN:
				this.closeAll();
				break;
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		final Player player = event.getPlayer();

		if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		if (event.getItem() == null || !event.getItem().hasItemMeta() || !event.getItem().getItemMeta().hasDisplayName())
			return;
		if (!event.getItem().getItemMeta().getDisplayName().equals(ITEM_NAME))
			return;
		if (FlashPlugin.getPlugin(FlashPlugin.class).getManager("Lobby", LobbyManager.class).getLobbyCountdown().getCount() < 10) {
			player.sendMessage(String.format("%s §cDafür ist es zu spät.", this.getPrefix()));
			return;
		}

		if (FlashPlugin.getPlugin(FlashPlugin.class).getState() != FlashPlugin.GameState.LOBBY_COUNTDOWN) {
			player.sendMessage(String.format("%s §cEs fehlen noch Spieler!", this.getPrefix()));
			return;
		}
		this.openInventory(player);
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();

		if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta() || !event.getCurrentItem().getItemMeta().hasDisplayName())
			return;
		if (event.getSlotType() != InventoryType.SlotType.CONTAINER)
			return;

		final FlashMap map = FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getMap(event.getCurrentItem().getItemMeta().getDisplayName());
		if (map == null)
			return;

		if (this.vote(FlashPlugin.getPlugin(FlashPlugin.class).getPlayer(player), map))
			player.sendMessage(String.format("%s §aDu hast erfolgreich für die Map §7%s §agestimmt.", this.getPrefix(), map.getName()));
		else
			player.sendMessage(String.format("%s §cDu hast bereits für diese Map abgestimmt!", this.getPrefix()));

		event.setCancelled(true);
	}

	protected void openInventory(Player player) {
		player.openInventory(this.inventory);
	}

	public void closeAll() {
		Bukkit.getOnlinePlayers().stream().filter(player -> player.getOpenInventory() != null && player.getOpenInventory().getTopInventory() != null
				&& player.getOpenInventory().getTopInventory().getTitle().equals(INVENTORY_NAME)).forEach(player -> player.closeInventory());
	}

	public void updateInventorys() {
		this.slotOfMap.forEach((slot, map) -> {
			ItemMeta meta = this.inventory.getItem(slot).getItemMeta();
			meta.setLore(Arrays.asList("§eErbauer: §a" + map.getAuthor(), "§eVotes: §a" + this.getVotes(map)));
			this.inventory.getItem(slot).setItemMeta(meta);
		});
		Bukkit.getOnlinePlayers().stream().filter(player -> player.getOpenInventory() != null && player.getOpenInventory().getTopInventory() != null
				&& player.getOpenInventory().getTopInventory().getTitle().equals(INVENTORY_NAME)).forEach(player -> player.updateInventory());
	}

	public boolean vote(FlashPlayer flashPlayer, FlashMap map) {
		if (flashPlayer.getVotedMap() == null || !flashPlayer.getVotedMap().equals(map)) {
			flashPlayer.setVotedMap(map);
			this.updateInventorys();
			return true;
		}
		return false;
	}

	public int getVotes(FlashMap map) {
		int votes = 0;

		for (FlashPlayer flashPlayer : FlashPlugin.getPlugin(FlashPlugin.class).getPlayers()) {
			if (flashPlayer.getVotedMap() != null && flashPlayer.getVotedMap().equals(map))
				votes++;
		}

		return votes;
	}

	public void pollVoting() {
		FlashMap highestVotes = null;
		int votes = -1;
		for (FlashMap map : FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getMaps()) {
			if (this.getVotes(map) > votes) {
				highestVotes = map;
				votes = this.getVotes(map);
			}
		}

		if (highestVotes == null) {
			Bukkit.broadcastMessage(String.format("%s §cEs konnte keine Map gefunden werden! Der Server wird gestoppt!",
					FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix()));
			Bukkit.shutdown();
			return;
		}
		
		this.closeAll();
		FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).setActualMap(highestVotes);
		Bukkit.broadcastMessage(String.format("%s §cDas Voting wurde beendet!", this.getPrefix()));
		Bukkit.broadcastMessage(String.format("%s §7Die Map §a%s §7hat das Voting gewonnen!", this.getPrefix(), highestVotes.getName()));
	}

}
