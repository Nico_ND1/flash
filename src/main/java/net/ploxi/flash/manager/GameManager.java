package net.ploxi.flash.manager;

import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.countdown.EndCountdown;
import net.ploxi.flash.countdown.FreezeCountdown;
import net.ploxi.flash.listener.CheckpointListener;
import net.ploxi.flash.map.FlashMap;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 *
 * @author RewisServer Development
 */
public class GameManager extends Manager {
	
	private final FreezeCountdown freezeCountdown = new FreezeCountdown();

	@Override
	public FlashPlugin.GameState[] activatedOn() {
		return FlashPlugin.GameState.ingameStates();
	}
	
	@Override
	public void onLoad() {
		this.registerListener(new CheckpointListener());
	}

	@Override
	public void onUnload() {
		this.unregisterAllListener();
	}

	@Override
	public void gameStart() {
		final FlashMap map = FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getActualMap();
		Bukkit.getOnlinePlayers().forEach(player -> {
			player.teleport(map.getSpawnLocation().toLocation());
			player.setWalkSpeed(map.getSpeed());
		});
		this.freezeCountdown.start();
	}

	@Override
	public void gameStop() {
		new EndCountdown().start();
		FlashPlugin.getPlugin(FlashPlugin.class).switchState(FlashPlugin.GameState.END);
	}

	@Override
	public void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState) {
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if(event.getCause() != EntityDamageEvent.DamageCause.FALL)
			event.setCancelled(true);
		event.setDamage(0);
	}

}
