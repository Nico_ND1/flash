package net.ploxi.flash.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.map.FlashMap;

/**
 *
 * @author RewisServer Development
 */
public class MapManager extends Manager {

	private final List<FlashMap> maps = new ArrayList<>();
	private FlashMap actualMap;

	public FlashMap getMap(String name) {
		return this.maps.stream().filter(map -> map.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	@Override
	public void onLoad() {
		File file = new File("plugins/Flash/Maps");
		file.mkdirs();

		for (File files : file.listFiles()) {
			FlashMap map = FlashMap.readFromConfig(files.getName());
			this.maps.add(map);
			System.out.println("Loaded map (" + map.getName() + ")");
		}
	}

	@Override
	public void onUnload() {
	}

	@Override
	public void gameStart() {
	}

	@Override
	public void gameStop() {
	}

	@Override
	public void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState) {
	}

	/**
	 * @return the maps
	 */
	public List<FlashMap> getMaps() {
		return maps;
	}

	/**
	 * @return the actualMap
	 */
	public FlashMap getActualMap() {
		return actualMap;
	}

	/**
	 * @param actualMap the actualMap to set
	 */
	public void setActualMap(FlashMap actualMap) {
		this.actualMap = actualMap;
	}

}
