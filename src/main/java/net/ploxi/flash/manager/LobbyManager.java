package net.ploxi.flash.manager;

import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.config.FlashGameConfig;
import net.ploxi.flash.countdown.LobbyCountdown;
import net.ploxi.flash.countdown.LobbyNoPlayersCountdown;
import net.ploxi.flash.util.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author RewisServer Development
 */
public class LobbyManager extends Manager implements Listener {

	private final LobbyCountdown lobbyCountdown = new LobbyCountdown();
	private final LobbyNoPlayersCountdown noPlayersCountdown = new LobbyNoPlayersCountdown();
	private final FlashGameConfig gameConfig = FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig();

	@Override
	public FlashPlugin.GameState[] activatedOn() {
		return FlashPlugin.GameState.lobbyStates();
	}

	@Override
	public void gameStart() {
		this.onUnload();
	}

	@Override
	public void gameStop() {
	}

	@Override
	public void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState) {
	}

	@Override
	public void onLoad() {
		this.registerListener(this);
	}

	@Override
	public void onUnload() {
		this.unregisterAllListener();
		this.getLobbyCountdown().stop();
		this.noPlayersCountdown.stop();
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		if (FlashPlugin.getPlugin(FlashPlugin.class).getState() != FlashPlugin.GameState.LOADING)
			return;
		event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§cDer Server lädt noch");
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		final int neededPlayers = this.gameConfig.getMinPlayers() - Bukkit.getOnlinePlayers().size();

		if (neededPlayers > 0) {
			this.getLobbyCountdown().stop();
			this.noPlayersCountdown.start(15 * 20);
		} else {
			this.getLobbyCountdown().start();
			this.noPlayersCountdown.stop();
		}

		event.setJoinMessage(String.format("%s §a%s §7hat das Spiel betreten. §8[§c%o§8/§c%o§8]", this.getPrefix(), player.getName(),
				Bukkit.getOnlinePlayers().size(), this.gameConfig.getMaxPlayers()));

		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
		player.setMaxHealth(20);
		player.setHealth(20);
		player.setFoodLevel(20);
		player.setLevel(0);
		player.setExp(0);
		player.setGameMode(GameMode.ADVENTURE);
		player.getActivePotionEffects().forEach((PotionEffect pot) -> player.removePotionEffect(pot.getType()));
		player.setTotalExperience(0);

		{
			player.getInventory().setItem(0, new ItemCreator().material(Material.PAPER).displayName("§eStimme für eine Map").build());
			player.getInventory().setItem(8, new ItemCreator().material(Material.SLIME_BALL).displayName("§cVerlasse das Spiel").build());
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final int neededPlayers = this.gameConfig.getMinPlayers() - (Bukkit.getOnlinePlayers().size() - 1);

		if (neededPlayers > 0) {
			this.getLobbyCountdown().stop(true);
			if (Bukkit.getOnlinePlayers().size() - 1 <= 0)
				this.getLobbyCountdown().resetCount();
			this.noPlayersCountdown.start(15 * 20);
		} else {
			this.noPlayersCountdown.stop();
			this.getLobbyCountdown().start();
		}

		event.setQuitMessage(String.format("%s §c%s §7hat das Spiel verlassen. §8[§c%o§8/§c%o§8]", this.getPrefix(), player.getName(),
				Bukkit.getOnlinePlayers().size() - 1, this.gameConfig.getMaxPlayers()));
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		event.setDeathMessage("");
		event.setDroppedExp(0);
		event.setKeepInventory(true);
		event.setKeepLevel(false);
		event.setNewExp(0);
		event.setNewLevel(0);
		event.setNewTotalExp(0);
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		event.setRespawnLocation(Bukkit.getWorld("world").getSpawnLocation());
	}

	/**
	 * @return the lobbyCountdown
	 */
	public LobbyCountdown getLobbyCountdown() {
		return lobbyCountdown;
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		event.setCancelled(true);
	}

}
