package net.ploxi.flash.manager;

import java.util.ArrayList;
import java.util.List;
import net.ploxi.flash.FlashPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 *
 * @author RewisServer Development
 */
public abstract class Manager {

	/**
	 * @see FlashPlugin#getManager(java.lang.String, java.lang.Class)
	 *
	 * @param <T> The type of the manager
	 * @param name The name of the manager
	 * @param clazz The class of the manager
	 * @return A casted manager class
	 */
	public static <T> T getManager(String name, Class<T> clazz) {
		return FlashPlugin.getPlugin(FlashPlugin.class).getManager(name, clazz);
	}

	private final List<Listener> listeners = new ArrayList<>();

	public abstract void onLoad();

	public abstract void onUnload();

	public abstract void gameStart();

	public abstract void gameStop();

	public abstract void switchState(FlashPlugin.GameState oldState, FlashPlugin.GameState newState);

	public FlashPlugin.GameState[] activatedOn() {
		return FlashPlugin.GameState.values();
	}

	public void registerListener(Listener listener) {
		synchronized (this.listeners) {
			if (this.listeners.contains(listener))
				return;
			Bukkit.getPluginManager().registerEvents(listener, FlashPlugin.getPlugin(FlashPlugin.class));
			this.listeners.add(listener);
		}
	}

	public void unregisterListener(Listener listener) {
		HandlerList.unregisterAll(listener);
		synchronized (this.listeners) {
			this.listeners.remove(listener);
		}
	}

	public void unregisterAllListener() {
		synchronized (this.listeners) {
			new ArrayList<>(this.listeners).forEach(listener -> this.unregisterListener(listener));
		}
	}

	public String getPrefix() {
		return FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix();
	}

}
