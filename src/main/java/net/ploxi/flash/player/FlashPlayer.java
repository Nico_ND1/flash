package net.ploxi.flash.player;

import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.map.FlashMap;
import org.bukkit.entity.Player;

/**
 *
 * @author RewisServer Development
 */
public class FlashPlayer {

	private final Player player;
	private int checkpointOrder = 0;
	private boolean ingame;

	// Vote-Manager
	private FlashMap votedMap;

	public FlashPlayer(Player player) {
		this.player = player;
	}

	public void sendMessage(String msg) {
		this.player.sendMessage(String.format("%s %s", FlashPlugin.getPlugin(FlashPlugin.class).getGameConfig().getPrefix(), msg));
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @return the checkpointOrder
	 */
	public int getCheckpointOrder() {
		return checkpointOrder;
	}

	/**
	 * @param checkpointOrder the checkpointOrder to set
	 */
	public void setCheckpointOrder(int checkpointOrder) {
		this.checkpointOrder = checkpointOrder;
	}

	/**
	 * @return the votedMap
	 */
	public FlashMap getVotedMap() {
		return votedMap;
	}

	/**
	 * @param votedMap the votedMap to set
	 */
	public void setVotedMap(FlashMap votedMap) {
		this.votedMap = votedMap;
	}

	/**
	 * @return the ingame
	 */
	public boolean isIngame() {
		return ingame;
	}

	/**
	 * @param ingame the ingame to set
	 */
	public void setIngame(boolean ingame) {
		this.ingame = ingame;
	}

}
