package net.ploxi.flash.map;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ploxi.flash.checkpoint.Checkpoint;
import net.ploxi.flash.util.ConfigurableLocation;
import net.ploxi.flash.util.Cuboid;

/**
 *
 * @author RewisServer Development
 */
public class FlashMap implements JsonDeserializer<FlashMap> {

	private static final Gson GSON = new GsonBuilder()
			.excludeFieldsWithoutExposeAnnotation()
			.serializeNulls()
			.setPrettyPrinting()
			.registerTypeAdapter(FlashMap.class, new FlashMap())
			.registerTypeAdapter(Checkpoint.class, new Checkpoint())
			.create();

	public static FlashMap readFromConfig(String name) {
		try {
			return GSON.fromJson(new InputStreamReader(new FileInputStream(new File("plugins/Flash/Maps/" + name + "/config.json")), Charset.forName("UTF-8")), FlashMap.class);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(FlashMap.class.getName()).log(Level.SEVERE, null, ex);
		}
		return new FlashMap();
	}

	@Expose private final String name;
	@Expose private final String author;
	@Expose private final float speed;
	@Expose private final String worldName;
	@Expose private final ConfigurableLocation spawnLocation;
	@Expose private ImmutableSet<Checkpoint> checkpoints;
	@Expose private final int dieAt;
	@Expose private final Map<String, Object> freezeCuboidMap;
	private final Cuboid freezeCuboid;

	public FlashMap(String name, String author, float speed, String worldName, ConfigurableLocation spawnLocation, ImmutableSet<Checkpoint> checkpoints, int dieAt, Map<String, Object> freezeCuboidMap) {
		this.name = name;
		this.author = author;
		this.speed = speed;
		this.worldName = worldName;
		this.spawnLocation = spawnLocation;
		this.checkpoints = checkpoints;
		this.dieAt = dieAt;
		this.freezeCuboidMap = freezeCuboidMap;
		this.freezeCuboid = new Cuboid(freezeCuboidMap);
	}

	public FlashMap() {
		this.name = null;
		this.author = null;
		this.speed = 0;
		this.worldName = null;
		this.spawnLocation = null;
		this.dieAt = 0;
		this.freezeCuboidMap = null;
		this.freezeCuboid = null;
	}

	@Override
	public FlashMap deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
		JsonObject jsonObject = je.getAsJsonObject();
		String author = jsonObject.get("author").getAsString();
		String name = jsonObject.get("name").getAsString();
		String worldName = jsonObject.get("worldName").getAsString();
		float speed = jsonObject.get("speed").getAsFloat();
		ConfigurableLocation spawnLocation = new ConfigurableLocation(jsonObject.get("spawnLocation").getAsJsonObject().get("x").getAsDouble(),
				jsonObject.get("spawnLocation").getAsJsonObject().get("y").getAsDouble(),
				jsonObject.get("spawnLocation").getAsJsonObject().get("z").getAsDouble(),
				jsonObject.get("spawnLocation").getAsJsonObject().get("yaw").getAsFloat(),
				jsonObject.get("spawnLocation").getAsJsonObject().get("pitch").getAsFloat(),
				jsonObject.get("spawnLocation").getAsJsonObject().get("world").getAsString());

		List<Checkpoint> list = new ArrayList<>();
		jsonObject.get("checkpoints").getAsJsonArray().forEach(element -> {
			list.add(jdc.deserialize(element, Checkpoint.class));
		});
		int dieAt = jsonObject.get("dieAt").getAsInt();
		Map<String, Object> map = GSON.fromJson(jsonObject.get("freezeCuboidMap"), new TypeToken<Map<String, Object>>() {
		}.getType());

		return new FlashMap(name, author, speed, worldName, spawnLocation, ImmutableSet.copyOf(list), dieAt, map);
	}

	public void save() {
		new File("plugins/Flash/Maps/" + this.name).mkdirs();
		try (FileWriter writer = new FileWriter(new File("plugins/Flash/Maps/" + this.name + "/config.json"))) {
			writer.write(GSON.toJson(this, FlashMap.class));
			writer.flush();
		} catch (IOException ex) {
			Logger.getLogger(FlashMap.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public Checkpoint getCheckpointByOrder(int order) {
		return this.getCheckpoints().stream().filter(checkpoint -> checkpoint.getOrder() == order).findAny().orElse(null);
	}

	public Checkpoint getNextCheckpoint(int actualOrder) {
		return this.getCheckpoints().stream().filter(checkpoint -> checkpoint.getOrder() == (actualOrder + 1)).findAny().orElse(null);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the authors
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @return the speed
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * @return the worldName
	 */
	public String getWorldName() {
		return worldName;
	}

	/**
	 * @return the spawnLocation
	 */
	public ConfigurableLocation getSpawnLocation() {
		return spawnLocation;
	}

	/**
	 * @return the checkpoints
	 */
	public ImmutableSet<Checkpoint> getCheckpoints() {
		return checkpoints;
	}

	/**
	 * @param checkpoints the checkpoints to set
	 */
	public void setCheckpoints(ImmutableSet<Checkpoint> checkpoints) {
		this.checkpoints = checkpoints;
	}

	/**
	 * @return the freezeCuboid
	 */
	public Cuboid getFreezeCuboid() {
		return freezeCuboid;
	}

	/**
	 * @return the dieAt
	 */
	public int getDieAt() {
		return dieAt;
	}

}
