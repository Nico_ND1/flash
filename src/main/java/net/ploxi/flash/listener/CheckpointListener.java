package net.ploxi.flash.listener;

import java.util.HashMap;
import java.util.Map;
import net.ploxi.flash.FlashPlugin;
import net.ploxi.flash.checkpoint.Checkpoint;
import net.ploxi.flash.manager.GameManager;
import net.ploxi.flash.manager.MapManager;
import net.ploxi.flash.map.FlashMap;
import net.ploxi.flash.player.FlashPlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author RewisServer Development
 */
public class CheckpointListener implements Listener {

	private final Map<Player, Block> blockMap = new HashMap<>();

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		final FlashPlayer flashPlayer = FlashPlugin.getPlugin(FlashPlugin.class).getPlayer(player);
		if(!flashPlayer.isIngame())
			return;
		final Block block = event.getTo().getBlock();
		Block oldBlock = this.blockMap.getOrDefault(player, null);
		if (oldBlock == null) {
			this.blockMap.getOrDefault(player, block);
			oldBlock = event.getFrom().getBlock();
		}
		if (block.equals(oldBlock))
			return;

		this.blockMap.put(player, block);
		final FlashMap map = FlashPlugin.getPlugin(FlashPlugin.class).getManager("Maps", MapManager.class).getActualMap();
		final Checkpoint foundCheckpoint = map.getCheckpoints().stream().filter(checkpoint -> checkpoint.contains(player.getLocation())).findAny().orElse(null);

		if (foundCheckpoint == null)
			return;
		final Checkpoint oldCheckpoint = map.getCheckpointByOrder(flashPlayer.getCheckpointOrder());
		if (oldCheckpoint != null && map.getNextCheckpoint(oldCheckpoint.getOrder()) != null
				&& !map.getNextCheckpoint(oldCheckpoint.getOrder()).equals(foundCheckpoint))
			return;

		foundCheckpoint.onEnter(flashPlayer);
		flashPlayer.setCheckpointOrder(foundCheckpoint.getOrder());

		if (foundCheckpoint.isFinisher()) {
			flashPlayer.sendMessage("§aDu hast das Ziel erreicht!");
			FlashPlugin.getPlugin(FlashPlugin.class).getManager("Game", GameManager.class).gameStop();
			return;
		}
		flashPlayer.sendMessage(String.format("§7Du hast den Checkpoint §aNummer %o §7erreicht.", foundCheckpoint.getOrder()));
	}

}
